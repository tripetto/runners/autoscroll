import { TRunnerViews } from "@tripetto/runner-react-hook";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "@tripetto/runner";
import { Block, Blocks } from "@ui/blocks";
import { BlockTitle } from "@ui/blocks/title";
import { BlockDescription } from "@ui/blocks/description";

export const EmptyMessage = (props: { readonly l10n: L10n.Namespace; readonly styles: IRuntimeStyles; readonly view: TRunnerViews }) => (
    <Blocks $styles={props.styles} $view={props.view} $center={true} $isMessage={true} $size={15}>
        <Block styles={props.styles} view={props.view} isPage={true} isMessage={true}>
            <BlockTitle $alignment="center">{props.l10n.pgettext("runner:autoscroll", "👋 Hi there!")}</BlockTitle>
            <BlockDescription $alignment="center">
                {props.l10n.pgettext("runner:autoscroll", "Please add a block first to get the magic going.")}
            </BlockDescription>
        </Block>
    </Blocks>
);
