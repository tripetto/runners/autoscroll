import { styled } from "styled-components";

export const BlockTitle = styled.h2<{
    $alignment?: "left" | "center" | "right";
}>`
    display: block;
    margin: 0;
    padding: 0;
    font-weight: bold;
    font-size: 2em;
    line-height: 1.2em;
    text-align: ${(props) => props.$alignment || "left"};
    user-select: none;
`;
