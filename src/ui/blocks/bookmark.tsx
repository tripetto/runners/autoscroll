import { styled } from "styled-components";

export const Bookmark = styled.div`
    display: block;
    position: absolute;
    left: 0;
    right: 0,
    top: 0;
    height: 20px;
    background-color: #000;
    color: #fff;
    border-radius: 5px;
`;
