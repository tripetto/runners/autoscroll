import { styled } from "styled-components";
import { useEffect } from "react";
import { useLoader } from "@helpers/resizer";

const ImageContainer = styled.div<{
    $aligment: "left" | "center" | "right";
}>`
    width: 100%;
    display: flex;
    align-items: flex-start;
    justify-content: ${(props) => props.$aligment};
`;

const ImageElement = styled.img`
    margin: 0;
    padding: 0;
    border: none;
    max-width: 100%;
    height: auto;
    border-style: none;
`;

export const BlockImage = (props: {
    readonly src: string;
    readonly isPage: boolean;
    readonly width?: string;
    readonly alignment?: "left" | "center" | "right";
    readonly onClick?: () => void;
}) => {
    const [onLoad, unmount] = useLoader(props.src);

    useEffect(() => {
        return () => unmount();
    });

    return (
        <ImageContainer onClick={props.onClick} $aligment={props.alignment || "left"}>
            <ImageElement
                src={props.src}
                style={{
                    width: props.width,
                }}
                onLoad={onLoad}
                onError={onLoad}
            />
        </ImageContainer>
    );
};
