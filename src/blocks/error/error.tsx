import { styled } from "styled-components";
import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Error } from "@tripetto/block-error/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";

const ErrorElement = styled.div<{
    $color: string;
}>`
    color: ${(props) => props.$color};
    transition: color 0.15s ease-in-out;

    > h2 {
        font-size: 1.6em;
        line-height: 1.3em;
    }
`;

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-error",
})
export class ErrorBlock extends Error implements IAutoscrollRendering {
    readonly hideButtons = true;

    render(props: IAutoscrollRenderProps): ReactNode {
        return (
            (props.name || props.description) && (
                <ErrorElement $color={props.styles.inputs.errorColor}>
                    {props.name}
                    {props.description}
                </ErrorElement>
            )
        );
    }
}
