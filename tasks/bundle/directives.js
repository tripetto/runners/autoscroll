const fs = require("fs");
const es5_path = "./runner/es5/index.js";
const es5 = fs.readFileSync(es5_path, "utf8").split("\n");
const esm_path = "./runner/esm/index.mjs";
const esm = fs.readFileSync(esm_path, "utf8").split("\n");

es5.splice(1, 0, `"use client";`);
esm.splice(1, 0, `"use client";`);

fs.writeFileSync(es5_path, es5.join("\n"), "utf8");
fs.writeFileSync(esm_path, esm.join("\n"), "utf8");
